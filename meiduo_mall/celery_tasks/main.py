# 1.导包
from celery import Celery

import os
if not os.getenv('DJANGO_SETTINGS_MODULE'):
    os.environ['DJANGO_SETTINGS_MODULE'] = 'meiduo_mall.settings.dev'


# 2.创建对象
celery_app = Celery('meiduo')

# 3.配置 消息队列
celery_app.config_from_object('celery_tasks.config')

# 4. 自动查找任务
celery_app.autodiscover_tasks(['celery_tasks.sms','celery_tasks.email'])
