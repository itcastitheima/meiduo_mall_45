from django.urls import path


from . import views

urlpatterns = [
    path('list/<int:category_id>/skus/',views.ListView.as_view()),

    path('hot/<int:category_id>/', views.HotView.as_view()),

    # 详情页
    path('detail/<int:sku_id>/', views.DetailView.as_view()),

    # 商品分类 日访问量
    path('detail/visit/<int:category_id>/', views.GoodsCategoryVisitView.as_view()),

    # 搜索路由--千万注意: 没有as_view()
    path('search/', views.MySearchView()),
]
