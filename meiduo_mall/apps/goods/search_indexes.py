# 1.导包
from haystack import indexes

from apps.goods.models import SKU


class SKUIndex(indexes.SearchIndex, indexes.Indexable):

    # 1,告诉 搜索引擎服务----去模板里面找 字段
    text = indexes.CharField(document=True, use_template=True)


    # 2.告诉 搜索引擎服务-哪张表 模型类
    def get_model(self):

        return SKU

    # 3.告诉 搜索引擎服务-哪张表 模型类里面的数据
    def index_queryset(self, using=None):
        return self.get_model().objects.filter(is_launched=True)
