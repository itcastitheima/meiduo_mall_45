from django import http
from django.shortcuts import render
from django.views import View
from haystack.views import SearchView

from apps.contents.utils import get_categories
from apps.goods.goods import get_breadcrumb, get_goods_specs
from apps.goods.models import GoodsCategory, SKU, GoodsVisitCount


class MySearchView(SearchView):

    # 返回结果  每页显示的个数
    results_per_page = 2

    def create_response(self):

        #  搜索引擎 返回来的数据格式
        context = self.get_context()


        # 转换前端的  数据格式
        skus_list = []

        for sku in context['page'].object_list:
            skus_list.append({
                'id': sku.object.pk,
                'name': sku.object.name,
                'price':sku.object.price,
                'default_image_url': sku.object.default_image.url,

                # 查询参数 值
                'searchkey': context['query'], # 'searchkey': self.query,
                # 总页数
                'page_size': context['paginator'].num_pages,
                # 总个数
                'count': context['paginator'].count,
                  # 'count':context['page'].paginator.count
            })
        return http.JsonResponse(skus_list, safe=False)



class GoodsCategoryVisitView(View):
    def post(self, request, category_id):
        # *   2. 校验 category 是否存在
        try:
            category = GoodsCategory.objects.get(pk=category_id)
        except:
            return http.JsonResponse({'code': 400, 'errmsg': "商品不存在!"})

        # *   3. 判断 当天的 日期 数据 是否存在
        from datetime import date
        today_date = date.today()

        try:
            goods_visit = GoodsVisitCount.objects.get(date=today_date, category=category)
        except:
            goods_visit = GoodsVisitCount()
            goods_visit.category = category

        try:
            goods_visit.count += 1
            goods_visit.save()
        except:
            return http.JsonResponse({'code': 400, 'errmsg': "记录失败!"})

        return http.JsonResponse({'code': 0, 'errmsg': "ok!"})

class DetailView(View):
    """商品详情页"""

    def get(self, request, sku_id):
        """提供商品详情页"""
        # 获取当前sku的信息
        try:
            sku = SKU.objects.get(id=sku_id)
        except SKU.DoesNotExist:
            return render(request, '404.html')

        # 查询商品频道分类
        categories = get_categories()
        # 查询面包屑导航
        breadcrumb = get_breadcrumb(sku.category)

        # 构建当前商品的规格
        goods_specs = get_goods_specs(sku)

        # 渲染页面
        context = {
            'categories': categories,
            'breadcrumb': breadcrumb,
            'sku': sku,
            'specs': goods_specs,
        }
        return render(request, 'detail.html', context)


class HotView(View):
    def get(self, request, category_id):
        # 3.接收参数
        # 4.校验category_id
        try:
            cat3 = GoodsCategory.objects.get(pk=category_id)
        except:
            return http.JsonResponse({'code': 400, 'errmsg': '商品不存在!'})

        # 5.根据category_id 查询对应的sku ---排序 销量降序-sales -- 切片[:2]
        skus = SKU.objects.filter(category=cat3).order_by('-sales')[0:2]

        # 6.python对象---[{}]
        sku_list = []
        for sku in skus:
            sku_list.append({
                'id': sku.pk,
                'name': sku.name,
                'price': sku.price,
                'default_image_url': sku.default_image.url,
            })
        # 7.返回响应
        return http.JsonResponse({'code': 0, 'errmsg': 'ok', 'hot_skus': sku_list})


class ListView(View):
    def get(self, request, category_id):
        # http://www.meiduo.site:8000/list/115/skus/?page=1&page_size=5&ordering=-create_time
        # 1.接收参数
        page = request.GET.get('page', 1)
        page_size = request.GET.get('page_size', 5)
        ordering = request.GET.get('ordering', '-create_time')

        # 2.校验参数--category_id  是否真的有
        try:
            cat3 = GoodsCategory.objects.get(pk=category_id)
        except Exception as e:
            print(e)
            return http.JsonResponse({'code': 400, 'errmsg': '商品不存在!'})

        # 获取 500个 商品分类 --现在的前端 没有渲染
        categories = get_categories()

        # 3.根据 cat3 去SKU表 查询对应的数据
        # skus = SKU.objects.filter(category=cat3).order_by(ordering)
        skus = SKU.objects.filter(category_id=category_id).order_by(ordering)

        # 分页
        from django.core.paginator import Paginator
        paginator = Paginator(skus, page_size)  # 实例化分页器
        num_pages = paginator.num_pages  # 总页数
        page_skus = paginator.page(page)  # 根据 页数 返回  对应的数据

        # p = Paginator(分的内容, 每页显示的个数)
        # p.num_pages 总页数
        # p.page(前端需要显示第几页)

        # 4.获取面包屑组件数据
        bread_crumb = get_breadcrumb(cat3)

        # 6.将python对象的查询集 数据---转换成 { [ {}]} json格式
        sku_list = []
        for sku in page_skus:
            sku_list.append({
                "id": sku.pk,
                "default_image_url": sku.default_image.url,
                "name": sku.name,
                "price": sku.price
            })
        data_dict = {
            'code': 0,
            'errmsg': 'ok',
            'list': sku_list,
            "count": num_pages,
            'breadcrumb': bread_crumb
        }
        # 7.返回前端
        return http.JsonResponse(data_dict)
