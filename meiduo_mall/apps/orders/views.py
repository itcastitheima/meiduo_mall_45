import json
import time
from decimal import Decimal
from datetime import datetime

from django import http
from django.shortcuts import render
from django.views import View
from django_redis import get_redis_connection

from apps.goods.models import SKU
from apps.orders.models import OrderInfo, OrderGoods
from apps.users.models import Address
from meiduo_mall.utils.views import LoginRequiredJSONMixin


class OrderCommitView(LoginRequiredJSONMixin, View):
    def post(self, request):
        # 1.参 address_id pay_method --body
        json_dict = json.loads(request.body)
        address_id = json_dict.get('address_id')
        pay_method = json_dict.get('pay_method')

        # 2.校 判空--判断地址 --判支付方式对不对
        if not all([address_id, pay_method]):
            return http.JsonResponse({'code': 400, 'errmsg': '参数必须填!'})
        try:
            address = Address.objects.get(pk=address_id)
        except:
            return http.JsonResponse({'code': 400, 'errmsg': '地址不对!'})

        if pay_method not in OrderInfo.PAY_METHODS_ENUM.values():
            return http.JsonResponse({'code': 400, 'errmsg': '支付方式不支持!'})

        from django.db import transaction
        with transaction.atomic():
            sid = transaction.savepoint()

            try:
                # 3.业
                # 3.1.orderinfo 表  OrderInfo.objects.create(8个字段)
                order_id = datetime.today().strftime('%Y%m%d%H%M%S') + '%09d' % request.user.pk
                order = OrderInfo.objects.create(order_id=order_id,
        user_id=request.user.pk,address_id=address_id,pay_method=pay_method,freight=Decimal(10.00),status=OrderInfo.ORDER_STATUS_ENUM['UNPAID'],
        total_count=0,total_amount=Decimal(0.00))

                # 3.2.订单商品表OrderGoods.objects.create(4个字段 sku_id)
                # 3.2.1 去购物车(redis)--selected--sku_id count selected
                client = get_redis_connection('carts')
                sku_redis = client.hgetall(request.user.pk)
                cart_dict = {}
                for k, v in sku_redis.items():
                    int_k = int(k.decode())
                    dict_v = json.loads(v)
                    if dict_v['selected']:
                        cart_dict[int_k] = dict_v

                for sku_id in cart_dict:

                    while True:
                        sku = SKU.objects.get(pk=sku_id)

                        # 老库存和销量
                        old_stock = sku.stock
                        old_sales = sku.sales

                        # 获取 商品 的 购买个数
                        cart_count = cart_dict[sku_id]['count']

                        # 当别人购买的时候--商家要看看库存有没有--如果有 才卖 , 没有 就卖不了
                        if cart_count > sku.stock:
                            # 回滚
                            transaction.savepoint_rollback(sid)
                            return http.JsonResponse({'code': 400, 'errmsg': f'{sku.name}-库存不足!'})


                        # 延迟 10s
                        # time.sleep(10)

                        # 库存减小 # 销量增加
                        new_stock = old_stock - cart_count
                        new_sales = old_sales + cart_count

                        # 乐观锁
                        result = SKU.objects.filter(pk=sku_id, stock=old_stock).update(stock=new_stock, sales=new_sales)

                        # 如果更新失败 -- 库存还有  继续下单
                        if result == 0:
                            continue

                        # 销量增加
                        sku.spu.sales += cart_count
                        sku.spu.save()

                        # 订单商品
                        OrderGoods.objects.create(order_id=order_id,sku=sku,count=cart_count,price=sku.price,)

                        # 计算总金额 -- 总个数
                        order.total_count += cart_count
                        order.total_amount += sku.price * cart_count

                        # 下单成功  跳出
                        break

                # 加个运费
                order.total_amount += order.freight
                order.save()
            except:
                transaction.savepoint_rollback(sid)
                return http.JsonResponse({'code': 400, 'errmsg': '下单失败!'})

        # 下单 成功 --清空购物车 选中的商品

        # 4.回
        return http.JsonResponse({'code': 0, 'errmsg': 'ok', 'order_id': order_id})


class OrderSettlemnetView(LoginRequiredJSONMixin, View):
    def get(self, request):
        # 1.取当前用户的地址
        addresses = Address.objects.filter(user=request.user, is_deleted=False)

        # 2.取购物车中 选中的 商品
        client = get_redis_connection('carts')
        sku_redis = client.hgetall(request.user.pk)
        cart_dict = {}
        for k, v in sku_redis.items():
            int_k = int(k.decode())
            dict_v = json.loads(v)
            if dict_v['selected']:
                cart_dict[int_k] = dict_v
        # 范围查询
        skus = SKU.objects.filter(id__in=cart_dict.keys())

        # 运费
        freight = Decimal(10.00)

        # 3.构建前端 需要的数据格式
        addresses_list = []
        for address in addresses:
            addresses_list.append({
                'id': address.id,
                'province': address.province.name,
                'city': address.city.name,
                'district': address.district.name,
                'place': address.place,
                'receiver': address.receiver,
                'mobile': address.mobile
            })

        sku_list = []
        for sku in skus:
            sku_list.append({
                'id': sku.id,
                'name': sku.name,
                'default_image_url': sku.default_image.url,
                'price': sku.price,

                'count': cart_dict[sku.id]['count'],
            })

        context = {
            'addresses': addresses_list,
            'skus': sku_list,
            'freight': freight,
            'default_address_id': request.user.default_address_id
        }
        # 4.回
        return http.JsonResponse({'code': 0, 'errmsg': 'ok', 'context': context})
