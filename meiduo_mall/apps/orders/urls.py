from django.urls import path


from . import views

urlpatterns = [
    path('orders/settlement/',views.OrderSettlemnetView.as_view()),

    path('orders/commit/', views.OrderCommitView.as_view()),
]
