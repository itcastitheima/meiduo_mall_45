import os

from django import http
from django.conf import settings
from django.shortcuts import render
from django.views import View
from alipay import AliPay

from apps.orders.models import OrderInfo
from apps.payment.models import Payment


class PaymentStatusView(View):
    def put(self, request):
        # 参
        data = request.GET.dict()

        # 剔除签名
        signatrue = data.pop('sign')

        # 校
        alipay = AliPay(
            appid=settings.ALIPAY_APPID,
            app_notify_url=None,
            app_private_key_path=os.path.join(os.path.dirname(os.path.abspath(__file__)), 'keys/app_private_key.pem'),
            alipay_public_key_path=os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                                'keys/alipay_public_key.pem'),
            sign_type="RSA2",
            debug=settings.ALIPAY_DEBUG,
        )
        success = alipay.verify(data, signatrue)

        # 业
        if success:

            order_id = data['out_trade_no']
            trade_id = data['trade_no']
            Payment.objects.create(
                order_id=order_id,
                trade_id=trade_id,
            )

            # 修改 订单状态
            OrderInfo.objects.filter(pk=order_id, status=1).update(status=2)
            # OrderInfo.objects.filter(pk=order_id).update(status=OrderInfo.ORDER_STATUS_ENUM['UNSEND'])
        # 回
        return http.JsonResponse({'code': 0, 'errmsg': 'OK!', 'trade_id': data['trade_no']})


class PaymentView(View):
    def get(self, request, order_id):

        # 参
        # 校
        try:
            order = OrderInfo.objects.get(pk=order_id, user=request.user, status=OrderInfo.ORDER_STATUS_ENUM['UNPAID'])
        except:
            return http.JsonResponse({'code': 400, 'errmsg': '订单不存在!'})

        # 业
        alipay = AliPay(
            appid=settings.ALIPAY_APPID,
            app_notify_url=None,
            app_private_key_path=os.path.join(os.path.dirname(os.path.abspath(__file__)), 'keys/app_private_key.pem'),
            alipay_public_key_path=os.path.join(os.path.dirname(os.path.abspath(__file__)), 'keys/alipay_public_key.pem'),
            sign_type="RSA2",
            debug=settings.ALIPAY_DEBUG,
        )

        # 加密参数
        order_string = alipay.api_alipay_trade_page_pay(
            subject="美多商城: %s" % order_id,
            out_trade_no=order_id,
            total_amount=str(order.total_amount),
            return_url=settings.ALIPAY_RETURN_URL,
        )

        # 拼接网址
        alipay_url = settings.ALIPAY_URL + "?" + order_string

        # 回
        return http.JsonResponse({'code': 0, 'errmsg': 'OK!', 'alipay_url': alipay_url})
