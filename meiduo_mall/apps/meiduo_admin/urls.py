from django.urls import path
from rest_framework_jwt.views import obtain_jwt_token
from apps.meiduo_admin.validate import admin_jwt_token

from apps.meiduo_admin.views.home import UserActiveCountView,UserOrderCountView,UserMonthAPIView
from apps.meiduo_admin.views.users import UserListAPIView
from apps.meiduo_admin.views.image import ImageModelViewSet,SimpleSKUListAPIView
from apps.meiduo_admin.views.sku import SKUModelViewSet,ThreeCategoryAPIView,SimpleSPUListAPIView,SPUSpecAPIView
from apps.meiduo_admin.views.permission import PermissionModelViewSet,ContentTypeListAPIView
from apps.meiduo_admin.views.group import GroupModelViewSet,SimplePermissionListAPIView
from apps.meiduo_admin.views.admin import AdminModelViewSet,SimpleGrpupListAPIView

urlpatterns = [
    # path('authorizations/',obtain_jwt_token),
    path('authorizations/',admin_jwt_token),

    # 获取日活用户量
    path('statistical/day_active/',UserActiveCountView.as_view()),

    # 获取日下单用户量
    path('statistical/day_orders/',UserOrderCountView.as_view()),

    # 获取月增用户图表数据
    path('statistical/month_increment/',UserMonthAPIView.as_view()),


    # 获取用户数据
    path('users/',UserListAPIView.as_view()),

    # 获取sku的所有数据
    path('skus/simple/',SimpleSKUListAPIView.as_view()),

    # 新增sku中  获取三级分类数据
    path('skus/categories/',ThreeCategoryAPIView.as_view()),

    # 新增sku中  获取所有spu数据
    path('goods/simple/',SimpleSPUListAPIView.as_view()),

    # 新增sku中  获取规格 和 选项
    path('goods/<spu_id>/specs/',SPUSpecAPIView.as_view()),

     # 获取所有 类型
    path('permission/content_types/',ContentTypeListAPIView.as_view()),

    # 获取所有权限
    path('permission/simple/',SimplePermissionListAPIView.as_view()),

    # 获取所有组
    path('permission/groups/simple/',SimpleGrpupListAPIView.as_view()),


]

############视图集的url是 通过 router来实现的######################


from rest_framework.routers import DefaultRouter
# 1.创建router实例
router=DefaultRouter()
# 2. 注册url
# url
# 一个列表视图的url        skus/images/
# 另外一个是 详情视图的url    skus/images/<pk>/
router.register('skus/images',ImageModelViewSet,basename='images')
# 3. 将router自动生成的url 追加到 urlpatterns中
urlpatterns += router.urls

###################################
# 2. 注册url
# url
# 一个列表视图的url        skus/
# 另外一个是 详情视图的url    skus/<pk>/
router.register('skus',SKUModelViewSet,basename='skus')
# 3. 将router自动生成的url 追加到 urlpatterns中
urlpatterns += router.urls



######权限的url#############################
# 2. 注册url
# url
# 一个列表视图的url        permission/perms/
# 另外一个是 详情视图的url    permission/perms/<pk>/
router.register('permission/perms',PermissionModelViewSet,basename='perms')
# 3. 将router自动生成的url 追加到 urlpatterns中
urlpatterns += router.urls


######组的url#############################
# 2. 注册url
# url
# 一个列表视图的url        permission/perms/
# 另外一个是 详情视图的url    permission/perms/<pk>/
router.register('permission/groups',GroupModelViewSet,basename='groups')
# 3. 将router自动生成的url 追加到 urlpatterns中
urlpatterns += router.urls


######组的url#############################
# 2. 注册url
# url
# 一个列表视图的url        permission/perms/
# 另外一个是 详情视图的url    permission/perms/<pk>/
router.register('permission/admins',AdminModelViewSet,basename='admins')
# 3. 将router自动生成的url 追加到 urlpatterns中
urlpatterns += router.urls




