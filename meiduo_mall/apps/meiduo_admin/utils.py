from rest_framework.pagination import PageNumberPagination,LimitOffsetPagination
from rest_framework.response import Response

# 继承父类,重写属性
class PageNum(PageNumberPagination):
    # 1. 开启分页
    # 2. 设置 一页显示多少个记录
    page_size = 2

    # 是我们在请求的时候 查询字符串的key值
    # 设置了值 之后, 允许我们 动态获取一页多少条记录
    page_size_query_param = 'pagesize'

    # 设置一页最大条数.防止 被攻击
    max_page_size = 50


    def get_paginated_response(self, data):


        return Response({
            'count':self.page.paginator.count,                              # 总共有多少条记录
            'lists':data,                                                   # 分页的列表数据
            'page':self.page.number,                                        # 第几页
            'pages':self.page.paginator.num_pages,                          # 总页数
            'pagesize':self.page_size                                       # 一页多少条记录
        })