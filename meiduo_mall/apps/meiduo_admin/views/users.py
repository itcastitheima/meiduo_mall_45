"""
我们在进行需求分析的时候, 把很多的需求分析出来之后, 一个个的实现.[罗马不是一天建成的]

例如: 我们的用户管理 有 新增用户 和用户展示. 我们先随便挑一个自己认为简单的实现

重复
用户展示.  用户展示 又有很多的需求. 我们要叫简化

1.  获取所有用户
2.  增加分页功能
3.  增加过滤搜索功能

"""

from rest_framework.generics import ListAPIView
from apps.users.models import User
from apps.meiduo_admin.serializers.users import UserModelSerializer
from rest_framework.response import Response
from apps.meiduo_admin.utils import PageNum

from rest_framework.generics import ListAPIView,CreateAPIView,ListCreateAPIView
# class UserListAPIView(ListAPIView):


from rest_framework.permissions import AllowAny,IsAuthenticated,IsAdminUser,IsAuthenticatedOrReadOnly

#对于模型的权限验证
from rest_framework.permissions import DjangoModelPermissions

class UserListAPIView(ListCreateAPIView):

    permission_classes = [
        DjangoModelPermissions
    ]

    # 默认是 所有数据
    # queryset = User.objects.all()
    # # 搜索的时候 应该是这样
    # queryset = User.objects.filter(username__contains='laowang')

    # 重写 或者 实现 get_queryset 方法
    # 可以根据不同的 业务逻辑,返回不同的查询结果集
    #  方法的作用 和  queryset 属性的作用是一页的
    def get_queryset(self):

        keywork=self.request.query_params.get('keyword')

        if keywork:
            return User.objects.filter(username__contains=keywork)

        return User.objects.all()

    serializer_class = UserModelSerializer

    # 设置分页类
    pagination_class = PageNum