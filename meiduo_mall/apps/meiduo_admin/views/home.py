from apps.users.models import User
from datetime import date
from rest_framework.response import Response


from rest_framework.views import APIView                        #基类
from rest_framework.generics import GenericAPIView              #一般和mixin配合使用
from rest_framework.generics import ListAPIView,RetrieveAPIView # 增删改查的请求方法都不用写
from rest_framework.viewsets import ModelViewSet                # 增删改查全都有

# 因为当前的需求 是没有用到序列化器的.所以 APIView ,GenericAPIView 甚至 ListAPIView 都可以
# 只不过 按照我们学习的 方式 我们选择 APIView 是最合适的.因为它什么都不需要
class UserActiveCountView(APIView):

    def get(self,request):
        # last_login   2021-3-8 15:15:15
        # 今天          2021-3-8
        today = date.today()
        count = User.objects.filter(last_login__gte=today).count()

        return Response({'count':count})


# 日下单用户

class UserOrderCountView(APIView):
    def get(self,request):

        today=date.today()
        count=User.objects.filter(orderinfo__create_time__gte=today).count()
        return Response({'count':count})

"""
获取 用户新增的 图表数据

1. 获取今天的日期
2. 往前推30天的日期
3. 遍历获取每一天的新增数量
    
    count= 时间是一个范围段
            1-23 00:00:00 ~ 1-24 00:00:00
[
    {"date":"1月3号"，“count":100},
    {"date":"1月4号"，“count":150},
    {"date":"1月5号"，“count":120}
]
"""
from datetime import timedelta

class UserMonthAPIView(APIView):

    def get(self,request):
        # 1.获取今天的日期
        today=date.today()
        # 2. 往前推30天的日期
        start_date= today - timedelta(days=30)

        data = []
        # 3. 遍历获取每一天的新增数量
        for i in range(30):
            #     count= 时间是一个范围段
            #             1-23 00:00:00 ~ 1-24 00:00:00
            gte_date= start_date + timedelta(days=i)
            lt_date= start_date + timedelta(days=(i+1))

            count=User.objects.filter(date_joined__gte=gte_date,date_joined__lt=lt_date).count()


            data.append({
                'count':count,
                'date':gte_date
            })

        return Response(data)
