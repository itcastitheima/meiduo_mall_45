from rest_framework.viewsets import ModelViewSet
from apps.meiduo_admin.serializers.admin import AdminUserModelSerialzier
from apps.users.models import User
from apps.meiduo_admin.utils import PageNum

class AdminModelViewSet(ModelViewSet):
    # is_super 超级管理员 肯定是 is_staff
    queryset = User.objects.filter(is_staff=True)

    serializer_class = AdminUserModelSerialzier

    pagination_class = PageNum

########获取所有组#####################
from rest_framework.generics import ListAPIView
from django.contrib.auth.models import Group
from apps.meiduo_admin.serializers.group import GroupModelSerializer
class SimpleGrpupListAPIView(ListAPIView):

    queryset = Group.objects.all()

    serializer_class = GroupModelSerializer