
"""
User 表 既包括 普通用户  也包括 管理员
"""
from apps.users.models import User
from django.contrib.auth.models import Permission
from django.contrib.auth.models import Group


from rest_framework.viewsets import ModelViewSet
from apps.meiduo_admin.utils import PageNum
from apps.meiduo_admin.serializers.permission import PermissionModelSerializer

class PermissionModelViewSet(ModelViewSet):

    queryset = Permission.objects.all()

    serializer_class = PermissionModelSerializer

    pagination_class = PageNum

######获取权限类型数据#######################
from django.contrib.auth.models import ContentType
from rest_framework.generics import ListAPIView
from apps.meiduo_admin.serializers.permission import ContentTypeModelSerializer

class ContentTypeListAPIView(ListAPIView):

    queryset = ContentType.objects.all()

    serializer_class = ContentTypeModelSerializer

