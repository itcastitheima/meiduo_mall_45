from apps.goods.models import SKU
from rest_framework.viewsets import ModelViewSet
from apps.meiduo_admin.serializers.sku import SKUModelSerializer
from apps.meiduo_admin.utils import PageNum


class SKUModelViewSet(ModelViewSet):

    queryset = SKU.objects.all()

    serializer_class = SKUModelSerializer

    pagination_class = PageNum


#########获取 三级分类数据########################
from rest_framework.views import APIView
from apps.goods.models import GoodsCategory
from rest_framework.response import Response
from apps.meiduo_admin.serializers.sku import GoodsCategoryModelSerializer
class ThreeCategoryAPIView(APIView):

    queryset = GoodsCategory.objects.filter(subs=None)

    def get(self,request):

        # 1. 获取三级分类查询结果集
        gcs = GoodsCategory.objects.filter(subs=None)

        # 2. 创建序列化器,将查询结果集转换为字典列表
        serializer=GoodsCategoryModelSerializer(gcs,many=True)
        # 3. 返回结果
        return Response(serializer.data)

#####获取所有的SPU数据#######################################

from apps.goods.models import SPU
from rest_framework.generics import ListAPIView
from apps.meiduo_admin.serializers.sku import SimpleSPUModelSerializer
class SimpleSPUListAPIView(ListAPIView):

    queryset = SPU.objects.all()

    serializer_class = SimpleSPUModelSerializer

    # 不需要分页

#######################################################

"""
1. 获取spu_id
2. 根据spu_id 获取 规格
3. 再根据规格 获取 选项      [选项 通过级联关系 可以获取到]
"""
from apps.goods.models import SPUSpecification
from apps.meiduo_admin.serializers.sku import SPUSpecificationModelSerializer
class SPUSpecAPIView(APIView):

    # queryset=SPUSpecification.objects.filter(spu_id=spu_id)
    def get_queryset(self):
        return SPUSpecification.objects.filter(spu_id=self.kwargs.get('spu_id'))

    def get(self,request,spu_id):

        # 获取到了 规格
        specs=SPUSpecification.objects.filter(spu_id=spu_id)

        serializer=SPUSpecificationModelSerializer(specs,many=True)

        return Response(serializer.data)



