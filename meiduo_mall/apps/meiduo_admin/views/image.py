from rest_framework.viewsets import ModelViewSet
from apps.goods.models import SKUImage
from apps.meiduo_admin.serializers.image import ImageModelSerializer
from apps.meiduo_admin.utils import PageNum
from rest_framework.response import Response
from apps.meiduo_admin.qiniu_upload import upload_data_to_qiniu
from django.conf import settings

class ImageModelViewSet(ModelViewSet):

    queryset = SKUImage.objects.all()

    serializer_class = ImageModelSerializer

    pagination_class = PageNum


    def create(self, request, *args, **kwargs):
        """
        1. 接收数据
        2. 验证数据

        接收二进制图片
        把二进制图片上传到七牛云中
        获取七牛云的 图片名字

        3. 数据入库
        4. 返回响应
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        # 1. 接收数据
        data=request.data
        sku_id=data.get('sku')
        # 2. 验证数据
        try:
            sku=SKU.objects.get(id=sku_id)
        except SKU.DoesNotExist:
            return Response({'code':'400','errmsg':'没有此商品'})
        #
        # 接收二进制图片
        upload_image=data.get('image')
        # 把二进制图片上传到七牛云中
        # read() 方法 可以帮助我们读取二进制流
        data=upload_image.read()
        # 获取七牛云的 图片名字
        image_str = upload_data_to_qiniu(data)

        # 3. 数据入库
        new_image=SKUImage.objects.create(
            sku=sku,
            image=image_str
        )
        # 4. 返回响应
        # 响应状态码 必须是 201
        # 前端就是根据 201响应状态码判断的
        return Response({
            'id':new_image.id,
            'sku':sku_id,
            'image': settings.QN_BASE_URL + image_str
        },status=201)


    def update(self, request, *args, **kwargs):
        """
        1. 获取到 要更新的指定数据
        2. 接收数据
        3. 验证数据

        获取图片二进制
        调用七牛云上传图片
        获取图片的名字

        4. 更新数据
        5. 返回响应
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        # 1. 获取到 要更新的指定数据
        # get_object() 方法 可以根据 url中的pk.查询到指定的数据
        instance=self.get_object()
        # 2. 接收数据
        sku_id=request.data.get('sku')
        # 3. 验证数据
        try:
            sku=SKU.objects.get(id=sku_id)
        except SKU.DoesNotExist:
            return Response({'code':400,'errmsg':'没有此商品'})
        #
        # 获取图片二进制
        new_image=request.data.get('image')
        # 调用七牛云上传图片
        data=new_image.read()
        # 获取图片的名字
        image_str=upload_data_to_qiniu(data)

        # 4. 更新数据
        instance.image=image_str
        # 如果 sku也更新了
        instance.sku=sku
        instance.save()
        # 5. 返回响应
        return Response({
            'id':instance.id,
            'sku':sku_id,
            'image':instance.image.url
        })





# 获取 所有sku数据
from rest_framework.generics import ListAPIView
from apps.goods.models import SKU
from apps.meiduo_admin.serializers.image import SimpleSKUModelSerializer

class SimpleSKUListAPIView(ListAPIView):

    queryset = SKU.objects.all()
    serializer_class = SimpleSKUModelSerializer
    # 不需要分页