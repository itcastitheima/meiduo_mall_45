from rest_framework.viewsets import ModelViewSet
from django.contrib.auth.models import Group
from apps.meiduo_admin.serializers.group import GroupModelSerializer
from apps.meiduo_admin.utils import PageNum


class GroupModelViewSet(ModelViewSet):

    queryset = Group.objects.all()

    serializer_class = GroupModelSerializer

    pagination_class = PageNum


# 权限列表,获取所有数据
from rest_framework.generics import ListAPIView
from django.contrib.auth.models import Permission
from apps.meiduo_admin.serializers.permission import PermissionModelSerializer

class SimplePermissionListAPIView(ListAPIView):

    queryset = Permission.objects.all()

    serializer_class = PermissionModelSerializer
