from rest_framework import serializers
from apps.goods.models import SKUImage

class ImageModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = SKUImage
        fields = '__all__'


##########
# 获取sku的序列化器
from apps.goods.models import SKU
class SimpleSKUModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = SKU
        fields = ['id','name']