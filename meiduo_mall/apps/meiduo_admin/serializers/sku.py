from apps.goods.models import SKU,SKUSpecification
from rest_framework import serializers

class SKUSpecModelSerializer(serializers.ModelSerializer):

    spec_id=serializers.IntegerField()
    option_id=serializers.IntegerField()

    class Meta:
        model = SKUSpecification
        fields = ['spec_id','option_id']


class SKUModelSerializer(serializers.ModelSerializer):

    category_id=serializers.IntegerField()
    category=serializers.StringRelatedField()

    # 前端也需要 spu和 spu_id
    spu_id=serializers.IntegerField()
    spu=serializers.StringRelatedField()

    specs = SKUSpecModelSerializer(many=True)

    class Meta:
        model = SKU
        fields = '__all__'

    def create(self, validated_data):
        # 1. 把 validated_data 中 多的数据 删除,并赋值给一个变量
        specs = validated_data.pop('specs')

        from django.db import transaction

        # with 语句会进行 自动提交 和 回滚
        with transaction.atomic():

            # 2. 保存sku数据
            sku=SKU.objects.create(**validated_data)
            # raise Exception('aaaaa')
            # 3. 遍历 多的数据, 保存 规格和选项
            for spec in specs:
                # spec = {'spec_id';xxx,'option_id':xxx}
                SKUSpecification.objects.create(sku=sku,**spec)


        return sku


    def update(self, instance, validated_data):

        # 1. 把 validated_data 数据 一分为2
        # 把 1对多的数据 拆分
        specs = validated_data.pop('specs')

        # 2.  specs = [{},{},]
        #     validated_data = {}
        # validated_data 是没有问题的数据,我们调用 父类
        # 让父类 帮助我们实现数据的更新操作
        super().update(instance,validated_data)
        # 3. 更新 多的一方 数据
        # 我们 不允许 修改 spu的值
        # 所以我们只更新 选项值
        # specs = [{spec_id: "1", option_id: 1}, {spec_id: "2", option_id: 3}, {spec_id: "3", option_id: 5}]
        for spec in specs:
            # spec = {spec_id: "1", option_id: 1}
            # 注意查询条件,要多一个 sku
            SKUSpecification.objects.filter(
                spec_id=spec.get('spec_id'),
                sku=instance
            ).update(
                option_id=spec.get('option_id')
            )
        return instance


#########获取 三级分类数据 序列化器########################
from apps.goods.models import GoodsCategory

class GoodsCategoryModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = GoodsCategory
        fields = ['id','name']

#####获取所有的SPU数据#######################################
from apps.goods.models import SPU
class SimpleSPUModelSerializer(serializers.ModelSerializer):

    class Meta:
        model =SPU
        fields = ['id','name']


###规格 和 选项 序列化器#####################################################
from apps.goods.models import SpecificationOption
class OptionModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = SpecificationOption
        fields = ['id','value']


# 规格序列化器
from apps.goods.models import SPUSpecification
class SPUSpecificationModelSerializer(serializers.ModelSerializer):

    options = OptionModelSerializer(many=True)

    class Meta:
        model = SPUSpecification
        fields = '__all__'


