from rest_framework import serializers
from apps.users.models import User
# serializers.ModelSerializer       -- 必须有模型  --自动生成字段和实现 create update方法
# serializers.Serializer

class UserModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        # fields = '__all__'      #偷懒的做法.测试的时候可以直接使用 .
        fields = ['id','username','mobile','email','password']

        extra_kwargs = {
            'password':{
                'write_only':True,
                'max_length':20,
                'min_length':5
            }
        }

    def create(self, validated_data):

        # 密码加密
        return User.objects.create_user(**validated_data)
