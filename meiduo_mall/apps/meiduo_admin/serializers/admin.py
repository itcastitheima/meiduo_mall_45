from rest_framework import serializers
from apps.users.models import User

class AdminUserModelSerialzier(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = '__all__'
        extra_kwargs = {
            'password':{
                'write_only':True
            }
        }

    def create(self, validated_data):
        # 1. 先调用父类方法
        # user = User.obects.create_user(**validate_data)
        user = super().create(validated_data)
        # 2. 补齐 缺少的功能
        # 2.1 密码加密
        user.set_password(validated_data.get('password'))
        # 2.2 添加一个 普通管理员权限
        user.is_staff=True
        user.save()
        return user
