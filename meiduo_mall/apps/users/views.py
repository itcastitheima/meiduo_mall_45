import re, json

from django import http
from django.contrib.auth import login, logout
from django.shortcuts import render
from django.views import View
from django_redis import get_redis_connection

from apps.goods.models import SKU
from apps.users.models import User, Address
from apps.users.utils import generate_verify_email_url
from meiduo_mall.settings.dev import logger
from meiduo_mall.utils.secret import SecretOauth
from meiduo_mall.utils.views import LoginRequiredJSONMixin
from celery_tasks.email.tasks import send_verify_email


class UserBrowserHistoryView(LoginRequiredJSONMixin, View):

    def post(self, request):

        # 参
        sku_id = json.loads(request.body).get('sku_id')
        # 校
        try:
            SKU.objects.get(pk=sku_id)
        except:
            return http.JsonResponse({'code': 400, 'errmsg': "商品不存在!"})

        # 业
        client = get_redis_connection('history')
        pipeline = client.pipeline()
        # 1.去重
        pipeline.lrem(request.user.pk, 0, sku_id)
        # 2.插入
        pipeline.lpush(request.user.pk, sku_id)
        # 3.截取5个
        pipeline.ltrim(request.user.pk, 0, 4)
        pipeline.execute()

        # 回
        return http.JsonResponse({'code': 0, 'errmsg': "oK!"})


    def get(self, request):

        # 业
        client = get_redis_connection('history')
        sku_ids = client.lrange(request.user.pk, 0, -1)

        # 回
        sku_list = []
        for sku_id in sku_ids:
            sku = SKU.objects.get(pk=sku_id)
            sku_list.append({
                'id': sku.pk,
                'name': sku.name,
                'price': sku.price,
                'default_image_url': sku.default_image.url,
            })

        return http.JsonResponse({'code':0, 'errmsg':'OK!', 'skus':sku_list})

class ChangePasswordView(LoginRequiredJSONMixin, View):
    def put(self, request):

        # 1.接收参数
        json_dict = json.loads(request.body)
        old_password = json_dict.get('old_password')
        new_password = json_dict.get('new_password')
        new_password2 = json_dict.get('new_password2')

        # 2.判空-判正则--判不一样---判老密码
        if new_password != new_password2:
            return http.JsonResponse({'code': 400, 'ermsg': '两次输入不一致'})

        if not request.user.check_password(old_password):
            return http.JsonResponse({'code': 400, 'ermsg': '密码不正确!'})

        # 3.修改新密码
        try:
            request.user.set_password(new_password)
            request.user.save()
        except Exception as e:
            logger.error(e)
            return http.JsonResponse({'code': 400, 'ermsg': '修改密码失败!'})

        # 4.退出 --删除cookie-username
        logout(request)

        response = http.JsonResponse({'code': 0, 'ermsg': '修改密码成功!'})
        response.delete_cookie('username')

        return response


# 13.默认标题
class AddressesTitleView(View):
    def put(self, request, address_id):

        title = json.loads(request.body).get('title')

        if not title:
            return http.JsonResponse({'code': 400, 'errmsg': '标题不能为空!'})

        try:
            Address.objects.filter(pk=address_id).update(title=title)
        except Exception as e:
            logger.error(e)
            return http.JsonResponse({'code': 400, 'errmsg': '设置标题失败!'})

        return http.JsonResponse({'code': 0, 'errmsg': '设置标题成功!'})


# 12.默认地址
class AddressesDefaultView(View):
    def put(self, request, address_id):

        try:
            request.user.default_address_id = address_id
            request.user.save()
        except Exception as e:
            logger.error(e)
            return http.JsonResponse({'code': 400, 'errmsg': '设置默认失败!'})

        return http.JsonResponse({'code': 0, 'errmsg': '设置默认成功!'})


# 11.修改 收货地址 /删除
class AddressesUpdateDeleteView(LoginRequiredJSONMixin, View):
    def put(self, request, address_id):
        # 1.接收参数
        json_dict = json.loads(request.body)
        receiver = json_dict.get('receiver')
        province_id = json_dict.get('province_id')
        city_id = json_dict.get('city_id')
        district_id = json_dict.get('district_id')
        place = json_dict.get('place')
        mobile = json_dict.get('mobile')
        tel = json_dict.get('tel')
        email = json_dict.get('email')

        # 2.校验--判空--正则--看地址是否有
        try:
            address = Address.objects.get(pk=address_id)
        except Exception as e:
            logger.error(e)
            return http.JsonResponse({'code': 400, 'errmsg': '地址不存在'})

        # 3.修改--update-save

        try:
            address.title = receiver
            address.receiver = receiver
            address.province_id = province_id
            address.city_id = city_id
            address.district_id = district_id
            address.place = place
            address.mobile = mobile
            address.tel = tel
            address.email = email

            address.save()
        except Exception as e:
            logger.error(e)
            return http.JsonResponse({'code': 400, 'errmsg': '修改失败!'})

        # 4.构建前端 需要的 数据格式
        address_dict = {
            "id": address.id,
            "title": address.title,
            "receiver": address.receiver,
            "province": address.province.name,
            "city": address.city.name,
            "district": address.district.name,
            "place": address.place,
            "mobile": address.mobile,
            "tel": address.tel,
            "email": address.email
        }

        return http.JsonResponse({'code': 0, 'errmsg': '修改成功', 'address': address_dict})

    def delete(self, request, address_id):

        try:
            address = Address.objects.get(pk=address_id)

            # 逻辑删除-- 修改--is_deleted = True
            address.is_deleted = True
            address.save()
        except Exception as e:
            logger.error(e)
            return http.JsonResponse({'code': 400, 'errmsg': '删除失败!'})

        return http.JsonResponse({'code': 0, 'errmsg': '删除成功!'})


# 10.查询 收货地址
class AddressesView(LoginRequiredJSONMixin, View):
    def get(self, request):
        # 1.查询 收货地址
        addresses = Address.objects.filter(user=request.user, is_deleted=False)

        # 2.构建前端 需要的数据格式
        addresses_list = []
        for address in addresses:
            addresses_list.append({
                "id": address.pk,
                "title": address.title,
                "receiver": address.receiver,
                "province": address.province.name,
                "city": address.city.name,
                "district": address.district.name,
                "place": address.place,
                "mobile": address.mobile,
                "tel": address.tel,
                "email": address.email
            })

        data_dict = {
            'code': 0,
            'errmsg': '展示地址',
            'addresses': addresses_list,
            'default_address_id': request.user.default_address_id,
            'limit': 20
        }

        # 3.返回
        return http.JsonResponse(data_dict)


# 9.新增 收货地址
class AddressCreateView(LoginRequiredJSONMixin, View):
    def post(self, request):

        # 判断如果 -- 个数 大于20, 不让新增

        #       地址表 (筛选用户)
        # count = Address.objects.filter(user=request.user, is_deleted=False).count()

        #       用户表(筛选自己的地址)
        count = request.user.addresses.filter(is_deleted=False).count()

        if count > 20:
            return http.JsonResponse({'code': 400, 'errmsg': '最多20个!'})

        # *   1.接收参数
        json_dict = json.loads(request.body)
        receiver = json_dict.get('receiver')
        province_id = json_dict.get('province_id')
        city_id = json_dict.get('city_id')
        district_id = json_dict.get('district_id')
        place = json_dict.get('place')
        mobile = json_dict.get('mobile')

        tel = json_dict.get('tel')
        email = json_dict.get('email')

        # *   2.校验 判空 -- 判断正则

        # *   3. 数据库存储 Address.objects.create()
        try:
            address = Address.objects.create(
                user=request.user,
                title=receiver,  # 默认标题 是收货人 名字

                province_id=province_id,
                city_id=city_id,
                district_id=district_id,

                receiver=receiver,
                place=place,
                mobile=mobile,
                tel=tel,
                email=email
            )

            # 设置 用户 默认地址
            # 1.判断  是否有
            if not request.user.default_address:
                request.user.default_address = address
                # request.user.default_address_id = address.pk
                request.user.save()

        except Exception as e:
            logger.error(e)
            return http.JsonResponse({'code': 400, 'errmsg': '新增地址失败!'})

        # *   4. 构建前端需要的 字典{}
        address_dict = {
            "id": address.pk,
            "receiver": address.receiver,

            "province": address.province.name,
            "city": address.city.name,
            "district": address.district.name,

            "place": address.place,
            "mobile": address.mobile,
            "tel": address.tel,
            "email": address.email,
        }

        return http.JsonResponse({'code': 0, 'errmsg': '新增地址成功!', 'address': address_dict})


# 8. 激活邮箱
class EmailVerifyView(View):
    def put(self, request):
        # 1.接收参数 token
        token = request.GET.get('token')
        if not token:
            return http.JsonResponse({'code': 400, 'errmsg': 'token必传'})
        # 2.解密
        data_dict = SecretOauth().loads(token)

        if not data_dict:
            return http.JsonResponse({'code': 400, 'errmsg': 'token无效!'})
        # 3.校验身份  和 邮箱
        try:
            user = User.objects.get(pk=data_dict['user_id'], email=data_dict['email'])
        except:
            return http.JsonResponse({'code': 400, 'errmsg': 'token无效!'})

        # 4.email_active=True
        user.email_active = True
        user.save()

        # 5. json_reponse
        return http.JsonResponse({'code': 0, 'errmsg': '邮箱激活成功!'})


# 7. 添加邮箱
class EmailView(LoginRequiredJSONMixin, View):
    def put(self, request):
        # 判断用户是否登录
        # 1.接收解析参数
        json_dict = json.loads(request.body)
        email = json_dict.get('email')

        # 2, 正则校验 邮箱
        if not email:
            return http.JsonResponse({'code': 400, 'errmsg': 'email缺少!'})
        if not re.match('^[a-z0-9][\w\.\-]*@[a-z0-9\-]+(\.[a-z]{2,5}){1,2}$', email):
            return http.JsonResponse({'code': 400, 'errmsg': 'email格式有误!'})

        # 3. 修改 User的 email字段
        request.user.email = email
        request.user.save()

        # 发邮件
        # 1.生成 激活链接
        verify_url = generate_verify_email_url(request)
        send_verify_email.delay(email, verify_url)

        # 4. 返回响应对象
        return http.JsonResponse({'code': 0, 'errmsg': "邮箱保存成功!"})


# 6. 个人中心
class InfoView(LoginRequiredJSONMixin, View):
    def get(self, request):
        data = {
            "code": 0,
            "errmsg": "ok",
            "info_data": {
                "username": request.user.username,
                "mobile": request.user.mobile,
                "email": request.user.email,
                "email_active": request.user.email_active
            }
        }
        return http.JsonResponse(data)


# 5. 退出功能
class LogOutView(View):
    def delete(self, request):
        # 退出
        logout(request)

        # 退出的时候  清空用户名
        response = http.JsonResponse({'code': 0, 'errmsg': '退出成功!'})
        response.delete_cookie('username')

        return response


# 4. 登录功能
class LoginView(View):
    def post(self, request):

        # 解析参数--校验--username password remembered
        json_dict = json.loads(request.body)
        username = json_dict.get('username')
        password = json_dict.get('password')
        remembered = json_dict.get('remembered')

        # 校验空 --判正则
        if not all([username, password]):
            return http.JsonResponse({'code': 400, 'errmsg': '参数不为空!'})

        # 根据username的参数值--手机号 --用户名
        if re.match('^1[3-9]\d{9}$', username):
            # 后端 用 mobile 判断
            User.USERNAME_FIELD = 'mobile'
        else:
            # 后端 用 username判断
            User.USERNAME_FIELD = 'username'

        # 业务逻辑---如何判断登录成功了---->拿着username--去数据库查询--有这个人--取出对应的pwd == 判断下
        from django.contrib.auth import authenticate
        # 如果登录成功 返回 user对象 否则 None
        user = authenticate(username=username, password=password)

        if user is None:
            return http.JsonResponse({'code': 400, 'errmsg': '用户名或密码错误!'})

        print("请求对象的User:", request.user)

        # 保持登陆状态
        login(request, user)

        print("请求对象的User:", request.user)

        # 是否记住登录
        if remembered:
            # 记住--14天
            request.session.set_expiry(None)
        else:
            # 不记住--会话结束
            request.session.set_expiry(0)

        # 设置 首页的用户名显示
        response = http.JsonResponse({'code': 0, 'errmsg': '登录成功!'})
        response.set_cookie('username', user.username, max_age=14 * 3600 * 24)

        # 返回响应对象
        return response


# 3.注册功能
class RegisterView(View):
    def post(self, request):
        try:
            # 1.接收参数
            json_dict = json.loads(request.body)

        except Exception as e:
            logger.error(e)
            return http.JsonResponse({'code': 400, 'errmsg': 'Json参数格式有误!'})

        # 2.解析参数
        username = json_dict.get('username')
        password = json_dict.get('password')
        password2 = json_dict.get('password2')
        mobile = json_dict.get('mobile')
        sms_code = json_dict.get('sms_code')
        allow = json_dict.get('allow')

        # 3.校验参数--判空--判断正则
        # # 3.1 用户名 --5-20位
        # if not re.match('^[a-zA-Z0-9_-]{5,20}$', username):
        #     return http.JsonResponse({'code': 400, 'errmsg': '请输入5-20个字符的用户名且不能为纯数字'})
        #
        # if re.match('^[0-9]+$', username):
        #     return http.JsonResponse({'code': 400, 'errmsg': '请输入5-20个字符的用户名且不能为纯数字'})
        # # 3.2 密码-- 8-20位
        # if not re.match('^[a-zA-Z0-9_@#$%^]{8,20}$', password):
        #     return http.JsonResponse({'code': 400, 'errmsg': '请输入8-20个字符的密码'})
        # # 3.3 两次密码是否一致
        # if password != password2:
        #     return http.JsonResponse({'code': 400, 'errmsg': '两次密码不一致!'})
        #
        # # 3.4 手机号 正则校验
        # if not re.match('^1[345789]\d{9}$', mobile):
        #     return http.JsonResponse({'code': 400, 'errmsg': '您输入的手机号格式不正确'})
        #
        # # 3.5 判断是否勾选了 同意
        # if not allow:
        #     return http.JsonResponse({'code': 400, 'errmsg': '请勾选同意!'})
        #
        #
        # # 校验 前端 和 后台的 短信验证码 是否一致
        # # 1. 链接redis
        # client = get_redis_connection('verify_code')
        # # 2. 取出后台的 短信验证码
        # redis_sms_code = client.get('sms_%s' % mobile)
        # # 3. 判断后台的 短信验证码  有还是没有
        # if not redis_sms_code:
        #     return http.JsonResponse({'code': 400, 'errmsg': '短信验证码过期!'})
        # # 4. 校验 前端 和 后台的 短信验证码 是否一致
        # if sms_code != redis_sms_code.decode():
        #     return http.JsonResponse({'code': 400, 'errmsg': '短信验证码有误!'})

        # 4.将用户数据存入数据库 --密码 将来 是 加密之后存储的 (Django自带的认证系统)

        try:
            # User.objects.create() 不能写 --密码 是明文
            from apps.users.models import User
            user = User.objects.create_user(username=username, password=password, mobile=mobile)

        except Exception as e:
            logger.error(e)  # 输出到 日志文件的
            return http.JsonResponse({'code': 400, 'errmsg': '注册失败!'})

        # 保持登录状态 -- request.session[k]=user.id
        from django.contrib.auth import login
        login(request, user)

        response = http.JsonResponse({'code': 0, 'errmsg': '注册成功!'})
        response.set_cookie('username', user.username, max_age=14 * 3600 * 24)

        # 5.返回响应
        return response


# 2. 作业--判断手机号是否重复

# 1. 判断用户名是否重复
class UsernameCountView(View):
    def get(self, request, username):
        # 1. 前端发送 get请求---usersname ---
        # 2.后台接收参数
        # 3.解析参数 --校验参数
        # if not re.match(r'^[a-zA-Z0-9]{5,20}$', username):
        #     return http.JsonResponse({'code': 400, 'errmsg':"用户名格式不正确"})

        # 4.业务逻辑--查询数据库 有没有这个人
        # try:
        #     user = User.objects.get(username=username)
        # except:
        #     return http.JsonResponse({'code': 400, 'errmsg':"用户不存在!"})

        count = User.objects.filter(username=username).count()

        # 5.返回前端 数据响应-- count
        return http.JsonResponse({'code': 0, 'count': count, 'errmsg': '用户已经存在!'})
