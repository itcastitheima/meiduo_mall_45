from django.urls import path, re_path


from . import views

urlpatterns = [

    # 1. 判断用户名是否重复
    # re_path('usernames/([a-zA-Z0-9]{5,20})/count/', views.UsernameCountView.as_view()),
    path('usernames/<username:username>/count/', views.UsernameCountView.as_view()),

    # 2. 作业--判断手机号是否重复

    # 3. 注册功能
    path('register/', views.RegisterView.as_view()),

    # 4. 登录功能
    path('login/', views.LoginView.as_view()),

    # 5. 退出功能
    path('logout/', views.LogOutView.as_view()),

    # 6. 用户中心
    path('info/', views.InfoView.as_view()),

    # 7. 添加邮箱
    path('emails/', views.EmailView.as_view()),

    # 8. 激活邮箱
    path('emails/verification/', views.EmailVerifyView.as_view()),

    # 9. 新增收货地址
    path('addresses/create/', views.AddressCreateView.as_view()),

    # 10.展示收货地址
    path('addresses/', views.AddressesView.as_view()),

    # 11.修改收货地址/删除
    path('addresses/<int:address_id>/', views.AddressesUpdateDeleteView.as_view()),

    # 12.修改默认地址/
    path('addresses/<int:address_id>/default/', views.AddressesDefaultView.as_view()),

    # 13.默认标题
    path('addresses/<int:address_id>/title/', views.AddressesTitleView.as_view()),

    # 14 修改密码
    path('password/', views.ChangePasswordView.as_view()),

    # 15.用户浏览记录
    path('browse_histories/', views.UserBrowserHistoryView.as_view()),
]
