from django.conf import settings

from meiduo_mall.utils.secret import SecretOauth


def generate_verify_email_url(request):
    """
    激活链接
    :param request:  请求对象
    :return:
    """

    # 1.激活邮箱 验证的 2个参数
    data_dict = {
        'user_id': request.user.pk,
        'email': request.user.email
    }
    # 2.加密
    token = SecretOauth().dumps(data_dict)

    # 3.拼接 全网址
    verify_url = settings.EMAIL_VERIFY_URL + token

    return verify_url