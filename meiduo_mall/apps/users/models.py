from django.db import models
from django.contrib.auth.models import AbstractUser

# 使用  Django的 认证系统-- User-AbstractUser--AbstractBaseUser
# username password email  last_login is_staff is_superuser date_joined
# 密码加密 解密功能


# 自定义  User模型类
from meiduo_mall.utils.models import BaseModel


class User(AbstractUser):
    #                        长度  11        唯一
    mobile = models.CharField(max_length=11, unique=True, verbose_name="手机号")

    # 新增 email_active 字段
    # 用于记录邮箱是否激活, 默认为 False: 未激活
    email_active = models.BooleanField(default=False,  verbose_name='邮箱验证状态')

    # 新增默认收货地址
    default_address = models.ForeignKey('Address', related_name='users', null=True, blank=True,
                                        on_delete=models.SET_NULL, verbose_name='默认地址')

    # 修改了表名
    class Meta:
        db_table = 'tb_users'

    # 魔法方法
    def __str__(self):
        return self.username



class Address(BaseModel):
    """
    用户地址
    """
    user = models.ForeignKey('users.User', on_delete=models.CASCADE, related_name='addresses', verbose_name='用户')
    province = models.ForeignKey('areas.Area', on_delete=models.PROTECT, related_name='province_addresses', verbose_name='省')
    city = models.ForeignKey('areas.Area', on_delete=models.PROTECT, related_name='city_addresses', verbose_name='市')
    district = models.ForeignKey('areas.Area', on_delete=models.PROTECT, related_name='district_addresses', verbose_name='区')

    title = models.CharField(max_length=20, verbose_name='地址名称')
    receiver = models.CharField(max_length=20, verbose_name='收货人')
    place = models.CharField(max_length=50, verbose_name='地址')
    mobile = models.CharField(max_length=11, verbose_name='手机')
    tel = models.CharField(max_length=20, null=True, blank=True, default='', verbose_name='固定电话')
    email = models.CharField(max_length=30, null=True, blank=True, default='', verbose_name='电子邮箱')

    is_deleted = models.BooleanField(default=False, verbose_name='逻辑删除')

    class Meta:
        db_table = 'tb_address'
        # 设置默认 已更新时间降序
        ordering = ['-update_time']

# 自己 定义 用户 的模型类
# class User(models.Model):
#     username = models.CharField(max_length=20, unique=True)
#     password = models.CharField(max_length=20)
#     mobile = models.CharField(max_length=11, unique=True)

# 密码的问题 -- 加密存储的 敏感数据-- 没有学过加密的知识点(现在问度娘)
# 登录的时候 --前端传的是明文密码---后端存的加密的 ---加密之后做对比
# 修改密码 --- 解密密码--明文--老密码--改新密码