import json

from django import http
from django.conf import settings
from django.contrib.auth import login
from django.shortcuts import render
from django.views import View

from QQLoginTool.QQtool import OAuthQQ

from apps.oauth.models import OAuthQQUser
from apps.users.models import User
from meiduo_mall.settings.dev import logger
from meiduo_mall.utils.secret import SecretOauth


# 判断 是否 绑定过 openid
def is_bind_openid(request, open_id):
    try:
        qq_user = OAuthQQUser.objects.get(openid=open_id)
    except OAuthQQUser.DoesNotExist:
        # 未绑定过 ----绑定页面--code:300
        # 1.将openid 传递--前端
        open_id = SecretOauth().dumps({'openid': open_id})
        response = http.JsonResponse({'code': 300, 'errmsg': "未绑定!", 'access_token': open_id})

    else:
        # 绑定过之后  --
        # 通过 qq_user 的外键属性 user 美多
        user = qq_user.user

        # 1.-保持登录状态 ---
        login(request, user)

        # 2.首页显示用户名字--设置cookie
        response = http.JsonResponse({'code': 0, 'errmsg': '绑定过'})
        response.set_cookie('username', user.username, max_age=14 * 24 * 3600)

        # 3.-重定向 到首页

    return response


# 2.接收QQ --code-->token-->openid
class QQCallBackView(View):
    def get(self, request):
        # 1.解析参数
        code = request.GET.get('code')

        # 2.校验参数
        if not code:
            return http.JsonResponse({'code': 400, 'errmsg': '缺少code参数!'})

        # 3.实例化 -QQ后台 --确认下美多后台的 身份
        oauth = OAuthQQ(
            client_id=settings.QQ_CLIENT_ID,
            client_secret=settings.QQ_CLIENT_SECRET,
            redirect_uri=settings.QQ_REDIRECT_URI,
            state=None
        )

        try:
            # 4. code--->token
            token = oauth.get_access_token(code)

            # 5. token-->openid
            open_id = oauth.get_open_id(token)
        except Exception as e:
            logger.error(e)
            return http.JsonResponse({'code': 400, 'errmsg': "openid获取失败!"})

        # 判断 是否 绑定过 openid
        response = is_bind_openid(request, open_id)

        return response

    def post(self, request):

        # 1.解析参数
        json_dict = json.loads(request.body)
        mobile = json_dict.get('mobile')
        password = json_dict.get('password')
        sms_code = json_dict.get('sms_code')
        open_id = json_dict.get('access_token')

        # 2.校验参数 --判空--判正则--判短信(作业)
        open_id = SecretOauth().loads(open_id).get('openid')
        # 2.判openid
        if not open_id:
            return http.JsonResponse({'code': 400, 'errmsg': 'open_id无效!'})

        # 3.判断 user
        try:
            user = User.objects.get(mobile=mobile)
        except:
            # 用户不存在--新建
            user = User.objects.create_user(username=mobile, password=password, mobile=mobile)
        else:
            # 判断密码是否正确
            if not user.check_password(password):
                return http.JsonResponse({'code': 400, 'errmsg': '密码不正确!'})

        # 4.绑定用户
        try:
            OAuthQQUser.objects.create(user=user, openid=open_id)
        except Exception as e:
            logger.error(e)
            return http.JsonResponse({'code': 400, 'errmsg': '绑定失败!'})

        # 保持登录状态
        login(request, user)

        # 设置cookie--username
        response = http.JsonResponse({'code': 0, 'errmsg': '绑定成功!'})
        response.set_cookie('username', user.username, max_age=14 * 24 * 3600)

        # 5.返回响应
        return response


# 1.获取 QQ 登录网址
class QQLoginUrlView(View):
    def get(self, request):
        # 1.实例化 认证对象---QQ后台 --确认下美多后台的 身份
        oauth = OAuthQQ(
            client_id=settings.QQ_CLIENT_ID,
            client_secret=settings.QQ_CLIENT_SECRET,
            redirect_uri=settings.QQ_REDIRECT_URI,
            state=None
        )

        # 2.获取QQ 登录网址
        login_url = oauth.get_qq_url()

        return http.JsonResponse({'code': 0, 'errmsg': 'qq网址', 'login_url': login_url})
