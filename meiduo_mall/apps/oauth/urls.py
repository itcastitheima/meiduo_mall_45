from django.urls import path


from . import views

urlpatterns = [

    # 1.获取 QQ 登录网址
    path('qq/authorization/',views.QQLoginUrlView.as_view()),

    # 2.接收QQ --code-->token-->openid
    path('oauth_callback/', views.QQCallBackView.as_view()),
]
