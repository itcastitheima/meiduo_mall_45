from django.urls import path


from . import views

urlpatterns = [

    # 获取省份
    path('areas/',views.AreaProvinceView.as_view()),

    # 获取 市区 --
    path('areas/<int:pk>/', views.AreaSubsView.as_view()),
]
