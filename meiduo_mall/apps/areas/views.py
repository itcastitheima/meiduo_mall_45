from django import http
from django.shortcuts import render
from django.views import View
from django.core.cache import cache

from apps.areas.models import Area

""" 
    SQL 省 select * from tb_areas where parent_id is NULL;
        市 select * from tb_areas where parent_id=130000;
        县 select * from tb_areas where parent_id=130100;
    
    ORM 省 Area.objects.filter(parent_id__isnull=True)
        市 Area.objects.filter(parent_id=130000)
        县 Area.objects.filter(parent_id=130100) 
"""


class AreaProvinceView(View):
    def get(self, request):
        # 1.从 缓存 取数据
        provinces_list = cache.get('provinces_list')

        # 2.如果没有 数据--才交互数据库
        if not provinces_list:

            print('只有第一次 才打印...')
            # 获取省份
            provinces = Area.objects.filter(parent_id__isnull=True)

            # 列表推导式 将 ORM的数据对象  ---> list/dict
            provinces_list = [{'id': pro.pk, 'name': pro.name} for pro in provinces]

            # 3. 获取数据 之后 存缓存
            cache.set('provinces_list', provinces_list, 3600)
        return http.JsonResponse({'code': 0, 'errmsg': '省份数据', 'province_list': provinces_list})


class AreaSubsView(View):
    def get(self, request, pk):

        # 1.获取 缓存的 数据
        sub_data = cache.get(f'subs_{pk}')

        # 2.如果没有缓存---mysql
        if not sub_data:
            # 1.获取 市/区 数据
            # subs = Area.objects.filter(parent_id=pk)
            # 下级 -- 上级 -- 县--市---省
            # sub = subs[0]
            # parent = sub.parent

            # 上级---下级---省--市--县
            parent = Area.objects.get(id=pk)
            subs = parent.subs.all()

            # 2.将 ORM的数据对象  ---> list/dict
            subs_list = []
            for sub in subs:
                subs_list.append({'id': sub.pk, 'name': sub.name})

            sub_data = {
                "id": parent.pk,
                'name': parent.name,
                "subs": subs_list
            }

            # 3. 写入缓存
            cache.set(f'subs_{pk}', sub_data, 3600)
        return http.JsonResponse({'code': 0, 'errmsg': '市区数据', 'sub_data': sub_data})
