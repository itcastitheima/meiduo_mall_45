from django.urls import path


from . import views

urlpatterns = [
    path('carts/',views.CartsView.as_view()),

    # 全选
    path('carts/selection/',views.CartsAllSelectView.as_view()),
]
