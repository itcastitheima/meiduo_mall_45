import json

from django import http
from django.shortcuts import render
from django.views import View
from django_redis import get_redis_connection

from apps.goods.models import SKU


class CartsAllSelectView(View):
    def put(self, request):
        # 1.参数 selected
        selected = json.loads(request.body).get('selected', True)

        # 2.校 isinstance(selected, bool)

        # 3.业  遍历所有的 商品--selected=True
        client = get_redis_connection('carts')
        sku_redis = client.hgetall(request.user.pk)

        # {b'1', b'{count:2,selected:True}'}
        for k, v in sku_redis.items():
            int_k = int(k.decode())
            dict_v = json.loads(v)

            # 修改 所有 商品 selected=True
            dict_v['selected'] = selected

            client.hset(request.user.pk, int_k, json.dumps(dict_v))

        # 4.回  jsonResponse()
        return http.JsonResponse({'code': 0, 'errmsg': 'ok!'})


class CartsView(View):
    def post(self, request):
        # 参
        json_dict = json.loads(request.body)
        sku_id = json_dict.get('sku_id')
        count = json_dict.get('count', 1)
        selected = json_dict.get('selected', True)

        # 校 --判空--数字--布尔--判断商品是否存在
        try:
            count = int(count)
        except:
            return http.JsonResponse({'code': 400, 'errmsg': "count必须是整数"})

        if not isinstance(selected, bool):
            return http.JsonResponse({'code': 400, 'errmsg': "selected必须是布尔"})
        try:
            sku = SKU.objects.get(pk=sku_id)
        except:
            return http.JsonResponse({'code': 400, 'errmsg': "商品不存在!"})
        # 业
        client = get_redis_connection('carts')

        # 1.去取出所有 的  数据
        sku_redis = client.hgetall(request.user.pk)

        # 2.判断当前商品 是否 已经存在
        # sku_redis = {b'10': b'{"count": 3, "selected": true}'}
        if str(sku_id).encode() in sku_redis:
            # 3.如果已经存在  count  累加
            sku_id = str(sku_id).encode()  # int 10 == > b'10'
            # 根据 b'10' 取出 b'{"count": 3, "selected": true}' ==> dict
            sku_dict = json.loads(sku_redis[sku_id])
            # 取出count 和 新的个数 累加
            sku_dict['count'] += count

            client.hset(request.user.pk, sku_id, json.dumps(sku_dict))
        else:
            # 4.如果不存在直接新增
            client.hset(request.user.pk, sku_id, json.dumps({'count': count, 'selected': selected}))

        # 回
        return http.JsonResponse({'code': 0, 'errmsg': "购物车新增成功"})

    def get(self, request):

        # 1.取所有  购物车的数据 sku_id count selected
        client = get_redis_connection('carts')
        # sku_ids_redis = {b'10': b'{"count": 3, "selected": true}'}
        sku_ids_redis = client.hgetall(request.user.pk)

        # 将 sku_ids_redis里面的   所有的  b'' --Python 的数据类型
        # cart_dict = {}
        # for k, v in sku_ids_redis.items():
        #     int_k = int(k.decode())
        #     dict_v = json.loads(v)
        #     cart_dict[int_k] = dict_v

        # 字典推导式
        cart_dict = {int(k.decode()): json.loads(v) for k, v in sku_ids_redis.items()}

        # 2.根据sku_id--->sku
        sku_ids = cart_dict.keys()

        # 3.构建 数据格式
        sku_list = []
        for sku_id in sku_ids:
            sku = SKU.objects.get(pk=sku_id)
            sku_list.append({
                'id': sku.pk,
                'name': sku.name,
                'price': sku.price,
                'default_image_url': sku.default_image.url,

                'count': cart_dict[sku_id]['count'],
                'selected': cart_dict[sku_id]['selected']
            })

        return http.JsonResponse({'code': 0, 'errmsg': "ok", 'cart_skus': sku_list})

    def put(self, request):
        # 参
        json_dict = json.loads(request.body)
        sku_id = json_dict.get('sku_id')
        count = json_dict.get('count', 1)
        selected = json_dict.get('selected', True)

        # 业
        try:
            client = get_redis_connection('carts')
            client.hset(request.user.pk, sku_id, json.dumps({'count': count, 'selected': selected}))
        except:
            return http.JsonResponse({'code': 400, 'errmsg': "bu ok!"})

        # 局部刷新
        cart_sku = {
            'count': count,
            'selected': selected,
            'sku_id': sku_id
        }
        return http.JsonResponse({'code': 0, 'errmsg': "ok!", 'cart_sku': cart_sku})

    def delete(self, request):
        # 参
        json_dict = json.loads(request.body)
        sku_id = json_dict.get('sku_id')

        try:
            sku = SKU.objects.get(pk=sku_id)
        except Exception as e:
            return http.JsonResponse({'code': 400, 'errmsg': "商品不存在!"})

        # 业
        try:
            client = get_redis_connection('carts')
            client.hdel(request.user.pk, sku_id)
        except:
            return http.JsonResponse({'code': 400, 'errmsg': "删除失败!!"})

        return http.JsonResponse({'code': 0, 'errmsg': "删除成功!"})
