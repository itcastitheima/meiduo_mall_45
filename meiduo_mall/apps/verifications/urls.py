from django.urls import path


from . import views

urlpatterns = [
    #image_codes/16d6dd84-f713-4b50-b813-2e843d4c8035/
    path('image_codes/<uuid:uuid>/', views.ImageCodeView.as_view()),

    # 发短信
    path('sms_codes/<mobile:mobile>/', views.SmsCodeView.as_view()),
]
