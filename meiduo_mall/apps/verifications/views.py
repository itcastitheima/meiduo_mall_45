import re, random

from django import http
from django.conf import settings
from django.shortcuts import render
from django.views import View
from django_redis import get_redis_connection



from meiduo_mall.settings.dev import logger


class SmsCodeView(View):
    def get(self, request, mobile):
        # 1.接收参数 --- mobile -- image_code---image_code_id
        image_code = request.GET.get('image_code')
        uuid = request.GET.get('image_code_id')

        # 2.校验参数 -- image_code_id正则
        if not all([image_code, uuid]):
            return http.JsonResponse({'code': 400, 'errmsg': '参数不齐!'})

        if not re.match('[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}', uuid):
            return http.JsonResponse({'code': 400, 'errmsg': '参数有误!'})

        # 3.校验图片验证码--前端--后端是否一致
        client = get_redis_connection('verify_code')
        redis_img_code = client.get('img_%s' % uuid)

        # 判断 后台的 图片验证码是否为空
        if redis_img_code is None:
            return http.JsonResponse({'code': 400, 'errmsg': '图片验证码已经失效!'})

        # 删除 后台  图片验证码
        client.delete('img_%s' % uuid)

        # 前端--后端是否一致 千万注意: redis取出来的 bytes
        if image_code.lower() != redis_img_code.decode().lower():
            return http.JsonResponse({'code': 400, 'errmsg': '图片验证码错误!'})


        # 获取 send_flag 的值
        send_flag = client.get(f'send_flag_{mobile}')

        if send_flag:
            return http.JsonResponse({'code': 400, 'errmsg': '短信太频繁!'})

        # 4.生成随机 6位 的数字
        sms_code = "%06d" % random.randint(1, 999999)
        sms_code = random.randint(100000, 999999)

        # 5.保存redis-数据库 2号 (mobile)k:v
        pipeline = client.pipeline()
        pipeline.setex('sms_%s' % mobile, 300, sms_code)
        pipeline.setex(f'send_flag_{mobile}', 60, 1)
        pipeline.execute()

        # 6.让第三方 云通讯 发短信
        from celery_tasks.sms.tasks import ccp_send_sms_code
        ccp_send_sms_code.delay(mobile, sms_code)

        logger.info(sms_code)

        # 7.返回响应对象
        return http.JsonResponse({'code': 0, 'errmsg': '发送成功!'})


class ImageCodeView(View):
    def get(self, request, uuid):
        # 1.生成图片验证码
        from apps.verifications.libs.captcha.captcha import captcha
        text, image_code = captcha.generate_captcha()

        # 2.存储到 Redis数据库-setex
        from django_redis import get_redis_connection
        client = get_redis_connection('verify_code')

        # 存储 图片验证码--有效时间 300s 5分钟
        # client.setex(str(uuid), 300, text)
        client.setex('img_%s' % uuid, 300, text)

        # 3.返回响应对象
        return http.HttpResponse(image_code, content_type='image/png')
