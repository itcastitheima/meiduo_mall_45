import os
import time

from django.conf import settings
from django.shortcuts import render
from django.template import loader

from apps.contents.models import ContentCategory
from apps.contents.utils import get_categories


def generate_static_index_html():
    print('%s: generate_static_index_html' % time.ctime())

    # 1.查询首页相关数据
    # 1.获取 商品分类数据
    categories = get_categories()

    # 2. 首页 广告数据
    contents = {}
    # 2.1 取出所有的 广告分类
    content_categories = ContentCategory.objects.all()

    # 2.2 根据分类 ---取广告内容 == 1:n  1.模型类_set.all()
    for category in content_categories:
        contents[category.key] = category.content_set.filter(status=True).order_by('sequence')

    # 8.构建前端数据
    context = {
        'categories': categories,
        'contents': contents
    }

    # 9.返回渲染的模板文件--- 数据 -- 自行渲染
    response = render(None, 'index.html', context)

    # 将首页html字符串写入到指定目录，命名'index.html'
    file_path = os.path.join(os.path.dirname(os.path.dirname(settings.BASE_DIR)),
                             'front_end_pc/index.html')
    with open(file_path, 'w') as f:
        f.write(response.content.decode())
