
from django.shortcuts import render
from django.views import View

from apps.contents.utils import get_categories
from apps.contents.models import ContentCategory


class IndexView(View):
    def get(self, request):

        # 1.获取 商品分类数据
        categories = get_categories()

        # 2. 首页 广告数据
        contents = {}
        # 2.1 取出所有的 广告分类
        content_categories = ContentCategory.objects.all()

        # 2.2 根据分类 ---取广告内容 == 1:n  1.模型类_set.all()
        for category in content_categories:
            contents[category.key] = category.content_set.filter(status=True).order_by('sequence')

        # 8.构建前端数据
        context = {
            'categories': categories,
            'contents': contents
        }
        # 9.返回渲染的模板文件--- 数据 -- 自行渲染
        return render(request, 'index.html', context)
