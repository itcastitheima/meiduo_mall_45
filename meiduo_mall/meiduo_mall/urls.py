"""meiduo_mall URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, register_converter

from meiduo_mall.utils.converters import MobileConverter, UserNameConverter

register_converter(MobileConverter, 'mobile')
register_converter(UserNameConverter, 'username')

urlpatterns = [
    path('admin/', admin.site.urls),

    # 1.用户模块
    path('', include('apps.users.urls')),

    # 2.验证码模块
    path('', include('apps.verifications.urls')),

    # 3.QQ登录模块
    path('', include('apps.oauth.urls')),

    # 4.areas模块
    path('', include('apps.areas.urls')),


    # 5.首页模块--测试的路由
    path('', include('apps.contents.urls')),

    # 6.商品模块
    path('',include('apps.goods.urls')),

    # 7.购物车模块
    path('',include('apps.carts.urls')),

    # 8.订单模块
    path('',include('apps.orders.urls')),

    # 9.支付模块
    path('',include('apps.payment.urls')),

    # 项目2
    # 我们项目2的 url 都是以 meiduo_admin 开头的
    # 所以 只要是满足条件的才能进入到子应用
    # meiduo_admin/authorizations/
    path('meiduo_admin/',include('apps.meiduo_admin.urls')),
]
