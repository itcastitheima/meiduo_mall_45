from django.conf import settings
from itsdangerous import TimedJSONWebSignatureSerializer as serializer


class SecretOauth(object):

    def __init__(self):
        self.ser = serializer(secret_key=settings.SECRET_KEY, expires_in=3600)

    # 1.加密
    def dumps(self, data):
        result = self.ser.dumps(data)
        return result.decode()

    # 2.解密
    def loads(self, data):
        try:
            result = self.ser.loads(data)
        except Exception as e:
            print(e)
            return None
        return result


"""
# 1.导包
from itsdangerous import TimedJSONWebSignatureSerializer

# 2.创建对象
s = TimedJSONWebSignatureSerializer(secret_key='fsdsfduu237878378358854', expires_in=15)

data_dict = {'a': "7EC"}

# 3. 加密==bytes
result = s.dumps(data_dict)
print(result.decode())


# 4.解密
loads_result = s.loads('eyJhbGciOiJIUzUxMiIsImlhdCI6MTYxMTgyMTQ3NSwiZXhwIjoxNjExODIxNDkwfQ.eyJhIjoiN0VDIn0.JkMbUyLVpTpctmbrONPJegbb43g6b2ZuHm2O-8YHje2G5gsc1vuWKwluyI1feNTW45ALMJwQDxU4ZPgn-ZxJ7A')
print(type(loads_result))
"""
