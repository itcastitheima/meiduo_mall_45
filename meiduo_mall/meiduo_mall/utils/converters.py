from django.urls import converters


# 手机号的 路由转换器
class MobileConverter:
    regex = '1[3-9]\d{9}'

    def to_python(self, value):
        return str(value)

    def to_url(self, value):
        return str(value)


# 用户名的 路由转换器
class UserNameConverter:
    regex = '[a-zA-Z0-9]{5,20}'

    def to_python(self, value):
        return str(value)

    def to_url(self, value):
        return str(value)
