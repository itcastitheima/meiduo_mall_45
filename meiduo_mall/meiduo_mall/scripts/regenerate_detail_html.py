#!/usr/bin/env python

import sys
sys.path.insert(0, '../../')

# 加载Django程序的环境
# 设置Django运行所依赖的环境变量
import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'meiduo_mall.settings.dev'

# 让Django进行一次初始化
import django
django.setup()

# 千万注意 项目导包一定放在最后
import os
from django.conf import settings
from django.shortcuts import render
from apps.contents.utils import get_categories
from apps.goods.goods import get_breadcrumb, get_goods_specs
from apps.goods.models import SKU


def generate_static_sku_detail_html(sku):
    # 查询首页相关数据
    # 查询商品频道分类
    categories = get_categories()
    # 查询面包屑导航
    breadcrumb = get_breadcrumb(sku.category)

    # 构建当前商品的规格
    goods_specs = get_goods_specs(sku)

    # 渲染页面
    context = {
        'categories': categories,
        'breadcrumb': breadcrumb,
        'sku': sku,
        'specs': goods_specs,
    }
    response = render(None, 'detail.html', context)

    # 将首页html字符串写入到指定目录，命名'1.html'
    file_path = os.path.join(os.path.dirname(os.path.dirname(settings.BASE_DIR)), f'front_end_pc/goods/{sku.pk}.html')
    print(file_path)
    with open(file_path, 'w') as f:
        f.write(response.content.decode())


if __name__ == '__main__':

    skus = SKU.objects.filter(is_launched=True)

    for sku in skus:
        generate_static_sku_detail_html(sku)
